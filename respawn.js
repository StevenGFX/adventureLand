function handle_death() {
  var respawnInterval = setInterval(function() {
    respawn();

    if (!character.rip) {
      clearInterval(respawnInterval);
    }
  }, 1000 * 10);

  return true;
} // end function
