function gameLogHandler(event) {
  let color;
  let message = event;
  if (
    event &&
    typeof event === "object" &&
    "color" in event &&
    "message" in event
  ) {
    message = event.message;
    color = event.color;
  }

  // If event is about gold income from loot
  if (color === "gold") {
    try {
      const gold_received = Number(
        message.replace(" gold", "").replace(/,/g, "")
      );
      // [Code to add to gold log]
    } catch (e) {
      console.log("Error porsing number from gold game_log event");
    }
  }

  // If event is about item drops from loot
  else if (color === "#4BAEAA") {
    try {
      // Example message: "Bjarne found an Amulet of HP" / "Bjarne found a Vitality Scroll"

      let looter;
      let item;
      if (character.party) {
        let splitted = message.split(" found a ");
        if (splitted.length === 1) {
          splitted = message.split(" found an ");
        }
        looter = splitted[0];
        item = splitted[1];
      } else {
        let splitted = message.split("Found a ");
        if (splitted.length === 1) {
          splitted = message.split("Found an ");
        }
        looter = character.name;
        item = splitted[1];
      }
      if (!looter || !item) throw "error";

      // [Code to add to loot log]
    } catch (e) {
      console.log("Error detecting loot in gameLog event handler");
    }
  }
}

parent.socket.on("gameLog", gameLogHandler);

// To remove the event handler on code stop
function on_destroy() {
  parent.socket.removeListener("gameLog", gameLogHandler);
  clear_drawings();
  clear_buttons();
}
