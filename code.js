var urls = [
  // set character look
  "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/setLook.js",

  // === Ops === //
  // loot
  "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/lootDrop.js",
  // low health
  "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/lowHealth.js",

  // === Attacking & Farming === //
  // attack/farming
  "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/attack.js",

  // === Drawings === //
  // show hp for mob and player
  "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/showHPBelowMobPlayer.js",
  // drawings
  // "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/drawings.js",

  // === Upgrade & Compound & Sell & exchange === //
  // auto upgrade/compound/sell/Exchange
  // "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/autoUpgradeCompoundSellExchange.js",

  // === GUI=== //
  // damage meter
  "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/dpsMeter.js",
  // gold/hr
  "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/goldMeter.js",
  // level up estimate
  "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/estTimeToLevelUp.js",
  // guis stuff
  "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/gui.js",
  // loot messages
  "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/lootMsgs.js"
  // hp calc
  // "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/basicXPCalc.js",
  // xp info
  // "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/xpData.js",

  // === Avoidance === //
  // vector entity avoidance
  // "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/vectorEntityAvoidance.js",
  // entity avoidance
  // "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/monsterAvoidance.js",

  // SHAME!
  //  "https://glcdn.githack.com/StevenGFX/adventureLand/raw/master/shame.js",
];

function loadURLs(url) {
  var ajax = new XMLHttpRequest();
  ajax.onreadystatechange = function() {
    var script = ajax.response || ajax.responseText;
    if (ajax.readyState === 4) {
      switch (ajax.status) {
        case 200:
          eval.apply(window, [script]);
          game_log("SUCCESS: Script Loaded! ", url);
          break;
        default:
          game_log("ERROR: Script Not Loaded. ", url);
      }
    }
  };
  ajax.open("GET", url, false); // <-- the 'false' makes it synchronous
  ajax.send(null);
}

urls.forEach(u => loadURLs(u));
