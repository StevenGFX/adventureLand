setInterval(function() {
  use_hp_or_mp();
  function alertLowHealth() {
    if (character.hp < 1000) {
      game_log("Warning: Low Health!", "red");
    }
  }
}, 1000 / 4); // Loops every 1/4 seconds.
