var your_attack = character.attack; //this is your characters average attack
var attack_mod; //armor/armorpiercing modifyer
var attack_mod2; //dreturn/reflection per second
var monster; //monster data
var monster_info;
var time_needed_to_kill; //number of attacks needed to kill monster
var time_needed;
var monster_respawn;
var monster_count;
var array = [];

for (mapID in parent.G.maps) {
  for (monsterID in parent.G.maps[mapID].monsters) {
    monster = parent.G.maps[mapID].monsters[monsterID];
    monster_info = parent.G.monsters[monster.type];
    monster_count = parent.G.maps[mapID].monsters[monsterID].count;
    monster_respawn = parent.G.monsters[monster.respawn];
    monster_boundary = parent.G.maps[mapID].monsters[monsterID].boundary;
    monster_boundary_center = boundary_center(
      monster_boundary[1],
      monster_boundary[2],
      monster_boundary[3],
      monster_boundary[4]
    );
    if ((monster_info.armor = null)) {
      monster_info.armor = 0;
    }
    if ((monster_info.evasion = null)) {
      monster_info.evasion = 0;
    }
    if ((monster_info.resistance = null)) {
      monster_info.resistance = 0;
    }
    if ((monster_info.reflection = null)) {
      monster_info.reflection = 0;
    }
    if ((monster_info.dreturn = null)) {
      monster_info.dreturn = 0;
    }
    if ((monster_info.lifesteal = null)) {
      monster_info.lifesteal = 0;
    }
    if ((character.apiercing = null)) {
      character.apiercing = 0;
    }
    if ((character.rpiercing = null)) {
      character.rpiercing = 0;
    }
    if ((character.reflection = null)) {
      character.reflection = 0;
    }
    if ((character.dreturn = null)) {
      character.dreturn = 0;
    }

    if (character.ctype == "warrior" || "ranger" || "rouge") {
      attack_mod = 1 + ((monster_info.armor - character.apiercing) / 1000) * -1;
    } else {
      attack_mod =
        1 + ((monster_info.resistance - character.rpiercing) / 1000) * -1;
    }

    if (monster_info.damage_type == "physical") {
      attack_mod2 =
        monster_info.attack *
        (character.dreturn / 100) *
        monster_info.frequency;
    } else {
      attack_mod2 =
        monster_info.attack *
        (character.reflection / 100) *
        monster_info.frequency;
    }

    time_needed_to_kill = Math.ceil(
      (1 + monster_info.evasion / 100) *
        (monster_info.hp / (your_attack * attack_mod * character.frequency) +
          attack_mod2)
    );
    xp_per_second = monster_info.xp / time_needed_to_kill;

    if (array.every(e => e[1])) {
      array.push([
        monster_info.name,
        monster_count,
        monster.type,
        xp_per_second,
        parent.G.maps[mapID].name,
        monster_boundary_center
      ]);
    }
  }
}

array.sort(function(a, b) {
  return b[2] - a[2];
});

function boundary_center(x1, y1, x2, y2) {
  var center_x = (x1 + x2) / 2;
  var center_y = (y1 + y2) / 2;

  return { x: center_x, y: center_y };
}

show_json(array);
