// source: @Dreous via Adventure.land Discord //

function setLook() {
  var lookSettings = {
    skin: "marmor11a",
    cx: {
      back: "backpacks201",
      beard: "beardo09",
      hat: "hat202",
      hair: "",
      head: "smakeup00",
      stone: "xgravestone1",
      upper: "marmor2b"
    }
  };

  for (var slot in lookSettings.cx) {
    var value = lookSettings.cx[slot];
    if (parent.character.cx[slot] != value) {
      parent.socket.emit("cx", {
        slot: slot,
        name: value
      });
    }
  }

  if (parent.character.skin != lookSettings.skin) {
    parent.socket.emit("cx", {
      slot: "skin",
      name: lookSettings.skin
    });
  }
}
setLook();
